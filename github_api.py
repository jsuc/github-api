import requests


class GitHub:

	def __init__(self, owner, repo, resources):
		_base_url = 'https://api.github.com/repos/{}/{}/{}'
		self._owner = owner
		self._urls = []
		self._page = 1
		self._index = 0		
		for repository in repo:  # Compiles the set of urls for each repo and resources 
			for resource in resources:
				self._urls.append(_base_url.format(owner, repository, resource))

	def __str__(self):
		return "<GitHub {}>".format(self._owner)

	def __move_to_next_url(self):
		"""Return a boolean
			
		Will return false if there are no longer available url
		"""
		index = self._index + 1
		if len(self._urls) > index:
			self._index += 1
			self._page = 1
			return True  
		else:
			return False

	def read(self):
		url = self._urls[self._index]
		print(url, 'page', self._page)
		response = requests.get(url, params={'page': self._page, 'per_page': 10})
		response = response.json()
		if len(response) > 0 and type(response) is list:  # validates if the response has no error message from the GitHub API
			self._page += 1
			return response
		else:
			continue_ = self.__move_to_next_url()
			if continue_:
				return self.read()
			else:
				return None