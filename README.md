# GitHub API

A small library that can ingest unlimited amounts of data from GitHub.
It streams batches of data from GitHub out to the caller (the user of your object). 
Also, it operates as a stream, or an iterable - such that every call to get more data, produces only a portion of that data (as the actual data can be arbitrarily large). And it enables it’s user to get multiple types of data on multiple repositories owned by the same owner/organization using a single API.



## Requirments

* **python 3.6.5^** - [Download](https://www.python.org/downloads/)
* **pip** python package manager - [Installation Tutorial](https://pip.pypa.io/en/stable/installing/)
* **git** version control - [Download](https://git-scm.com/downloads)

## Setup

```bash
$ git clone https://jsuc@bitbucket.org/jsuc/github-api.git
```
```bash
$ cd github-api/
```
```bash
$ pip install -r requirements.txt 
```


## Usage

```python
from github_api import GitHub

def stream_moby_repo():
	gh = GitHub(owner="moby", 
				repositories=["moby", "buildkit", "tool"], 
				resources=["issues", "commits", "pulls"]) 
	data = gh.read()
	while data is not None:     
		data = gh.read()	# fetch next batch 
 
	
```

## Documentation

#### Class Arguments
* **owner** `string` - A string representing the ​*Owner/Organization*​ name

* **repo** `list` - List of strings representing the *Repository* names

* **resources** `list` - List of desired `Resources`

```python

foo_gh = GitHub(owner="foo",
				repositories=["foo", "bar"],
				resources=["commits", "issues"])

```

#### List of Supported Resources 
* **issues** `string` - List of issues 

* **pulls** `string` - List of pull requests 

* **commits** `string` - List of commits

```python
resources = ["issues", "pulls", "commits"]
```

#### Methods
* **.read()** - *Takes No Arguments*

Each call to ​read​ will return a portion of data: a list of data points returned from the API (e.g a list of dictionaries)
The class must iterate of every repository and resource combination as the user calls ​read() multiple times. 

#### Test Case
You will need `pytest` in this area. See the docs [here](http://doc.pytest.org/en/latest/contents.html)

```bash
$ python -m pytest -v test.py 
```
or 
```bash
$ py.test -v test.py
```