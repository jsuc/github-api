from github_api import GitHub


def test_single_data():
    gh = GitHub(owner='torvalds', 
    			repo=['linux'], 
    			resources=['commits'])
    data = gh.read()
    assert(len(data) > 0)

def test_streaming():
	loop = 0
	gh = GitHub(owner='torvalds',
				repo=['linux'],
				resources=['commits'])
	data = gh.read()
	while data is not None and loop > 2:
		data = gh.read()
		loop += 1

def test_fake_repo():
	gh = GitHub(owner='777', 
				repo=['aa'], 
				resources=['commits'])
	data = gh.read()
	assert(data is None)